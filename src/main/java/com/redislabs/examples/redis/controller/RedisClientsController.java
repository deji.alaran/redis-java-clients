package com.redislabs.examples.redis.controller;

import javax.naming.NamingException;

import com.redislabs.examples.redis.service.RedisClientsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RedisClientsController {

	@Autowired
	private RedisClientsService pocService;

	@GetMapping("/key/{key}")
	public String get(@PathVariable String key) {
		System.out.println("getting " + key);
		String value = "{\"" + key + "\":\"" + pocService.get(key) + "\"}";
		return value;
	}
}
