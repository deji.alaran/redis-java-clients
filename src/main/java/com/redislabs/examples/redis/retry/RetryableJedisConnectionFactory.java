package com.redislabs.examples.redis.retry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.retry.RetryContext;
import org.springframework.retry.support.RetryTemplate;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Connection factory creating
 * <a href="http://github.com/xetorthio/jedis">Jedis</a> based connections.
 * Connections are retried based on the configured RetryTemplate.
 * <p>
 * {@link JedisConnectionFactory} should be configured using an environmental
 * configuration and the {@link JedisClientConfiguration client configuration}.
 * Jedis supports the following environmental configurations:
 * <ul>
 * <li>{@link RedisStandaloneConfiguration}</li>
 * <li>{@link RedisSentinelConfiguration}</li>
 * <li>{@link RedisClusterConfiguration}</li>
 * </ul>
 *
 * @see JedisClientConfiguration
 * @see Jedis
 */
public class RetryableJedisConnectionFactory extends JedisConnectionFactory {

    private static final Logger LOG = LoggerFactory.getLogger(RetryableJedisConnectionFactory.class);

    private RetryTemplate retryTemplate;

    public RetryableJedisConnectionFactory() {
    }

    public RetryableJedisConnectionFactory(JedisPoolConfig poolConfig) {
        super(poolConfig);
    }

    public RetryableJedisConnectionFactory(RedisSentinelConfiguration sentinelConfig) {
        super(sentinelConfig);
    }

    public RetryableJedisConnectionFactory(RedisSentinelConfiguration sentinelConfig, JedisPoolConfig poolConfig) {
        super(sentinelConfig, poolConfig);
    }

    public RetryableJedisConnectionFactory(RedisClusterConfiguration clusterConfig) {
        super(clusterConfig);
    }

    public RetryableJedisConnectionFactory(RedisClusterConfiguration clusterConfig, JedisPoolConfig poolConfig) {
        super(clusterConfig, poolConfig);
    }

    public RetryableJedisConnectionFactory(RedisStandaloneConfiguration standaloneConfig) {
        super(standaloneConfig);
    }

    public RetryableJedisConnectionFactory(RedisStandaloneConfiguration standaloneConfig,
            JedisClientConfiguration clientConfig) {
        super(standaloneConfig, clientConfig);
    }

    public RetryableJedisConnectionFactory(RedisSentinelConfiguration sentinelConfig,
            JedisClientConfiguration clientConfig) {
        super(sentinelConfig, clientConfig);
    }

    public RetryableJedisConnectionFactory(RedisClusterConfiguration clusterConfig,
            JedisClientConfiguration clientConfig) {
        super(clusterConfig, clientConfig);
    }

    /**
     * The RetryTemplate that will be used to retry connections.
     *
     * @return the retryTemplate
     */
    public RetryTemplate getRetryTemplate() {
        return retryTemplate;
    }

    /**
     * Sets the RetryTemplate used to retry connections.
     *
     * @param retryTemplate the retryTemplate; can be {@code null}
     */
    public void setRetryTemplate(RetryTemplate retryTemplate) {
        this.retryTemplate = retryTemplate;
    }

    /**
     * Returns a Jedis instance to be used as a Redis connection. The instance can
     * be newly created or retrieved from a pool. When there is an exception
     * connecting it will be retried according to the {@code retryTemplate}.
     *
     * @return Jedis instance ready for wrapping into a {@link RedisConnection}.
     */
    @Override
    protected Jedis fetchJedisConnector() {
        if (retryTemplate != null) {
            return retryTemplate.execute((RetryContext context) -> {
                if (context.getRetryCount() > 0) {
                    LOG.warn("Retrying Redis connection. Retry Count = {}", context.getRetryCount());
                }
                return super.fetchJedisConnector();
            });
        }
        return super.fetchJedisConnector();
    }

}