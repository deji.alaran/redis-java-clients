package com.redislabs.examples.redis.config;

import java.net.UnknownHostException;
import java.time.Duration;

import com.redislabs.examples.redis.retry.RetryableJedisConnectionFactory;

import org.springframework.context.annotation.Profile;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.retry.RetryPolicy;
import org.springframework.retry.backoff.BackOffPolicy;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

import redis.clients.jedis.JedisPoolConfig;

@Configuration
public class RedisDataConfig {
	@Value("${app.redis.host}")
	public String redisHost;

	@Value("${app.redis.port}")
	public int redisPort;

	@Value("${app.jedis.pool.max-total}")
	public int maxTotal;

	@Value("${app.jedis.pool.max-active}")
	public int maxActive;

	@Value("${app.jedis.pool.max-idle}")
	public int maxIdle;

	@Value("${app.jedis.pool.min-idle}")
	public int minIdle;

	@Value("${app.jedis.pool.max-wait-millis}")
	public long maxWaitMillis;

	@Value("${app.jedis.pool.time-between-eviction-runs-millis}")
	public long timeBetweenEvictionRunsMillis;

	private static final Duration CONNECTION_RETRY_BACKOFF_INITIAL_INTERVAL = Duration.ofMillis(200);

	private static final Duration CONNECTION_RETRY_BACKOFF_MAX_INTERVAL = Duration.ofSeconds(1);

	private static final double CONNECTION_RETRY_BACKOFF_MULTIPLIER = 1.5;

	private static final int CONNECTION_RETRY_MAX_ATTEMPTS = 10;

	private static final Duration CONNECTION_READ_TIMEOUT = Duration.ofSeconds(10);

	private static final Duration CONNECTION_TIMEOUT = Duration.ofSeconds(10);

	@Bean(name = "connectionFactory")
	@Profile({ "jedis", "jedis-cf" })
	public RedisConnectionFactory redisConnectionFactory() throws UnknownHostException {
		JedisPoolConfig poolConfig = new JedisPoolConfig();
		poolConfig.setMaxTotal(maxTotal);
		poolConfig.setMaxIdle(maxIdle);
		poolConfig.setMinIdle(minIdle);
		poolConfig.setMaxWaitMillis(maxWaitMillis);
		poolConfig.setTestWhileIdle(true);
		poolConfig.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);

		System.out.println("connecting to " + redisHost + " @ " + redisPort);
		RedisStandaloneConfiguration config = new RedisStandaloneConfiguration(redisHost, redisPort);
		
		// If you have a password
		// config.setPassword(RedisPassword.of("redis"));

		JedisConnectionFactory factory = new JedisConnectionFactory(config,
				JedisClientConfiguration.builder().connectTimeout(Duration.ofMillis(3000L))
						.readTimeout(Duration.ofMillis(1000L)).usePooling().poolConfig(poolConfig)
						.build());

		return factory;
	}

	@Bean(name = "redisTemplate")
	@Profile({ "jedis" })
	public RedisTemplate<String, String> redisTemplate(RedisConnectionFactory connectionFactory) {
		return createTemplate(connectionFactory);
	}

	@Bean(name = "connectionFactory")
	@Profile({ "jedis-rf" })
	public RedisConnectionFactory jedisRFConnectionFactory() {
		JedisPoolConfig poolConfig = new JedisPoolConfig();
		poolConfig.setMaxTotal(maxTotal);
		poolConfig.setMaxIdle(maxIdle);
		poolConfig.setMinIdle(minIdle);
		poolConfig.setMaxWaitMillis(maxWaitMillis);
		poolConfig.setTestWhileIdle(true);
		poolConfig.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);

		RedisStandaloneConfiguration config = new RedisStandaloneConfiguration(redisHost, redisPort);

		RetryableJedisConnectionFactory factory = new RetryableJedisConnectionFactory(config,
				JedisClientConfiguration.builder().connectTimeout(Duration.ofMillis(3000L))
						.readTimeout(Duration.ofMillis(1000L)).usePooling().poolConfig(poolConfig).build());
		factory.setRetryTemplate(sessionRetryTemplate());
		return factory;
	}

	private RetryTemplate sessionRetryTemplate() {
		RetryTemplate retryTemplate = new RetryTemplate();
		retryTemplate.setBackOffPolicy(backOffPolicy());
		retryTemplate.setRetryPolicy(retryPolicy());
		return retryTemplate;
	}

	private BackOffPolicy backOffPolicy() {
		ExponentialBackOffPolicy defaultBackOffPolicy = new ExponentialBackOffPolicy();
		defaultBackOffPolicy.setInitialInterval(CONNECTION_RETRY_BACKOFF_INITIAL_INTERVAL.toMillis());
		defaultBackOffPolicy.setMaxInterval(CONNECTION_RETRY_BACKOFF_MAX_INTERVAL.toMillis());
		defaultBackOffPolicy.setMultiplier(CONNECTION_RETRY_BACKOFF_MULTIPLIER);
		return defaultBackOffPolicy;
	}

	private RetryPolicy retryPolicy() {
		SimpleRetryPolicy defaultRetryPolicy = new SimpleRetryPolicy(CONNECTION_RETRY_MAX_ATTEMPTS);
		return defaultRetryPolicy;
	}

	@Bean(name = "connectionFactory")
	@Profile({ "lettuce", "lettuce-cf" })
	public RedisConnectionFactory lettuceConnectionFactory() {
		RedisStandaloneConfiguration config = new RedisStandaloneConfiguration(redisHost, redisPort);
		return new LettuceConnectionFactory(config);
	}

	@Bean(name = "redisTemplate")
	@Profile({ "lettuce", "lettuce-cf" })
	public RedisTemplate<String, String> redisTemplateLettuce(RedisConnectionFactory connectionFactory) {
		return createTemplate(connectionFactory);
	}

	private RedisTemplate<String, String> createTemplate(RedisConnectionFactory connectionFactory) {
		RedisTemplate<String, String> template = new RedisTemplate<>();
		template.setConnectionFactory(connectionFactory);
		return template;
	}
}