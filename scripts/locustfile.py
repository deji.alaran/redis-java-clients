from locust import HttpLocust, TaskSet, task, between, web
import locust.events
from random import seed, randint
import time
import atexit

class SimpleLocustTest(TaskSet):

    @task
    def get_something(self):
        self.client.get("/examples/key/key")

class LocustTests(HttpLocust):
    seed(1)
    task_set = SimpleLocustTest
    wait_time = between(0, 1)

    def __init__(self):
        super(LocustTests, self).__init__()
